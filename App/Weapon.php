<?php

class Weapon extends Item {

    /** @var int */
    protected $atkPoint;

    /** @var int */
    protected $magicAtkPoint;

    public function __construct(string $name, int $atkPoint, int $magicAtkPoint)
    {
        parent::__construct($name);
        
        $this->atkPoint = $atkPoint;
        $this->magicAtkPoint = $magicAtkPoint;
    }

    public function getAtkPoint() {
        return $this->atkPoint;
    }
}