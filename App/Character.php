<?php

class Character {


    /** @var string $name */
    protected $name;

    /** @var int $hp */
    protected $hp;

    /** @var int $maxHp */
    protected $maxHp;

    /** @var int $atk */
    protected $atk;

    /** @var int $magicAtk */
    protected $magicAtk;

    /** @var int $armor */
    protected $armor;

    /** @var int $armor */
    protected $magicArmor;

    /** @var Inventory */
    protected $inventory;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->inventory = new Inventory;

        $this->hp = 50;
        $this->maxHp = 50;
        $this->atk = 10;
        $this->magicAtk = 5;
        $this->armor = 10;
        $this->magicArmor = 5;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function getArmor()
    {
        $total = $this->armor + $this->getInventory()->getTotalArmor();
        return $total;
    }

    public function setArmor(int $value)
    {
        $this->armor = $value;
    }

    public function getMagicArmor()
    {
        return $this->magicArmor;
    }

    public function getAtk()
    {
        $total = $this->atk + $this->getInventory()->getTotalAtk();
        return $total;
    }

    /**
     * @return int
     */
    public function getMaxHp(): int
    {
        return $this->maxHp;
    }

    /**
     * @param int $maxHp
     */
    public function setMaxHp(int $maxHp): void
    {
        $this->maxHp = $maxHp;
    }

    public function getHp()
    {
        return $this->hp;
    }

    public function getMagicAtk()
    {
        return $this->magicAtk;
    }

    public function setHp(int $hp)
    {
        $this->hp = $hp;
    }

    /**
     * Attaque une cible
     * @param object $target
    */
    public function attack(object $target): void
    {
        if($target->isDead()) {
            echo "<p>Je vais quand même pas taper sur un macabé</p>";
        } else {
            $damage = $this->getAtk() - $target->getArmor();
            if ($damage > 0) {
                $target->loseHp($damage);
            }
        }
    }

    /**
     * Permet de perdre le nombre de vie placée en paramètre
     */
    public function loseHp(int $toLose)
    {
        $actualHp = $this->getHp();
        $actualHp = $actualHp - $toLose;

        $this->setHp($actualHp);

        if($this->isDead()) {
            echo "<p>Le personnage est mort</p>";

            $this->setHp(0);
        }
    }

    /**
     * Le personnage gagne le nombre d'hp placé en param sans
     * dépacer le maximum
     * @param int $toWin
     */
    public function winHp(int $toWin)
    {
        $actualHp = $this->getHp();
        $actualHp = $actualHp + $toWin;

        if ($actualHp >= $this->maxHp) {
            $this->setHp($this->maxHp);
        } else {
            $this->setHp($actualHp);
        }
    }

    /**
     * Permet de savoir si le personnage est toujours vivant
     * @return bool
     */
    protected function isDead(): bool
    {
        if ($this->getHp() <= 0) {
            return true;
        } else {
            return false;
        }
    }

}