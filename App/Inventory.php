<?php

class Inventory {

    /** @var Weapon */
    protected $weaponSlot;

    /** @var Helmet */
    protected $helmetSlot;

    /** @var Chest */
    protected $chestSlot;

    /**
     * Equipe automatiquement l'item dans l'emplacement de son type
     * @param Item $item
     */
    public function equipItem(Item $item)
    {
        if($item instanceof Weapon) {
            $this->weaponSlot = $item;
        }

        if($item instanceof Chest) {
            $this->chestSlot = $item;
        }
    }

    public function getTotalAtk()
    {

        if ($this->weaponSlot instanceof Weapon) {
            return $this->weaponSlot->getAtkPoint();
        }

        return 0;
    }

    /**
     * Retourne le total du bonus d'armure de l'inventaire
     */
    public function getTotalArmor()
    {

        $totalArmor = 0;


        if ($this->chestSlot instanceof Chest) {
            $totalArmor += $this->chestSlot->getArmorPoint();
        }

        return $totalArmor;
    }
}