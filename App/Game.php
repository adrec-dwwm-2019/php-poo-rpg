<?php


class Game
{
    const MAX_PLAYER = 5;
    const MIN_PLAYER = 1;

    const NAME_LIST = ['Arthur', 'Perceval', 'Merlin', 'Kadoc', 'Karadoc', 'Igerne', 'Duchesse D\'aquitaine'];

    const AUTHORIZE_CLASS = [
        Warrior::class,
        Archer::class,
        Wizard::class,
    ];

    /**
     * @param int $playerCount
     * @return array
     */
    public static function init(int $playerCount)
    {
        $playerArray = [];

        for ($i = 0; $i < $playerCount; $i++) {
            $playerArray[] = self::generateRandomPlayer();
        }

        return $playerArray;
    }

    public static function generateRandomPlayer()
    {
        $player = NULL;
        $type = rand(0, 2);
        $name = self::NAME_LIST[array_rand(self::NAME_LIST, 1)];

        if ($type === 0) {
            $player = new Warrior($name);
        }
        if($type === 1) {
            $player = new Wizard($name);
        }
        if($type === 2) {
            $player = new Archer($name);
        }


        return $player;
    }
}