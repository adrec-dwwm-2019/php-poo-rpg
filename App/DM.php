<?php

class DM
{
    public static function dStats(Character $character)
    {
        /**
         * Nom -> HP : 50/100 | Armor : 220 | Atk: 20 |
         */

        return $character->getName() . ' -> HP :' . $character->getHp() . '/' . $character->getMaxHp() . ' | Armor : ' . $character->getArmor() . ' | Atk : ' . $character->getAtk();
    }

    /**
     * @param string $text
     * @param string $prefix
     * @param string $suffix
     */
    public static function enclose(string $text, string $prefix = '<p>', string $suffix ='</p>')
    {
        echo $prefix . $text . $suffix;
    }

}

