<?php 


class Warrior extends Character {

    public function __construct(string $name) {
        parent::__construct($name);
        $this->hp = 40;
        $this->maxHp = 80;
        $this->atk = 20;
        $this->magicAtk = 2;
        $this->armor = 15;
        $this->magicArmor = 2;
    }

    /**
     * Pour le guerrier, ajouter une méthode d'attaque sur cible multiple.
     * Les points d'attaque sont répartie de façon uniforme sur toutes les cibles.
     * Bonus de 30% sur l'attaque
     * @param array $targets
     */
    public function circularAttack(array $targets)
    {
        $targetsCount = count($targets);
        $totalDamage = intval($this->getAtk() * 1.3);
        $damagePerTarget = intval($totalDamage / $targetsCount);
        
        foreach ($targets as $target) {
            if($target->isDead()) {
                echo "<p>Je vais quand même pas taper sur un macabé</p>";
            } else {

                $damage = $damagePerTarget - $target->getArmor();

                if ($damage > 0) {
                    $target->loseHp($damage);
                }
            }
        }
    }

}