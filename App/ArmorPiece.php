<?php

class ArmorPiece extends Item {

    /** @var int */
    protected $armorPoint;

    /** @var int */
    protected $magicArmorPoint;

    public function __construct(
        string $name, 
        int $armorPoint = 0, 
        int $magicArmorPoint = 0
        )
    {
        parent::__construct($name);
        $this->armorPoint = $armorPoint;
        $this->magicArmorPoint = $magicArmorPoint;
    }

    public function getArmorPoint()
    {
        return $this->armorPoint;
    }
}

