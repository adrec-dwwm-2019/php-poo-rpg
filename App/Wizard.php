<?php

class Wizard extends Character {
    public function __construct(string $name) {
        parent::__construct($name);
        $this->hp = 20;
        $this->maxHp = 40;
        $this->atk = 4;
        $this->magicAtk = 15;
        $this->armor = 5;
        $this->magicArmor = 10;
    }

    /**
     * Le mage peut soigner une cible de la moitié de ces point d'attaque
     * @param object $target
     */
    public function healTarget(object $target): void
    {
        $target->winHp(intval($this->magicAtk / 2));
    }

    //Methode drain de vie (drainLife()). Permet au mage de faire perdre 
    //autant de point à l'adversaire que le mage a d'attaque magique.
    //Le mage récupère entre 50 et 75% des points de vie pour se heal
    //lui même
        
    public function drainLife(object $target): void
    {
        if($target->isDead()) {
            echo "<p>Je vais quand même pas drainer du sang pourri</p>";
        } else {

            $damage = $this->getMagicAtk() - $target->magicArmor;

            if ($damage > 0) {
                $target->loseHp($damage);

                $heal = intval($damage * (rand(50, 75) / 100));

                $this->winHp($heal);
            }
        }
    }

}