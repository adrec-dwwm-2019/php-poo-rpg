<?php

class Car {

    /**
     * @var int $tankSize
     */
    private $tankSize = 60;

    /**
     * @var float $inTankFuel
     */
    private $inTankFuel = 40;

    /**
     * Conso litres / 100km
     * @var float $uptake
     */
    private $uptake = 5;

    /**
     * @var float $totalKm
     */
    private $totalKm = 152000;


    public function course(float $distance): void
    {
        $this->totalKm += $distance;
        $this->inTankFuel -= $this->consumes($distance);
    }

    /**
     * Retourne la quantité de carburant consommé
     */
    private function consumes(float $distance): float
    {
        $uptakePerKm = $this->uptake / 100;
        return $uptakePerKm * $distance;
    }

}