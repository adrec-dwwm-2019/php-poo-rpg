<?php

class Archer extends Character {

    /*
        1- D'initaliser l'archer avec des stats cohérentes
        2- Methode HeavyBowAttack qui consiste en un tir deux fois plus puissant
        mais qui n'a que 2 chance sur 3 de faire mouche
        3- Methode reduceArmor() qui fait perdre 1 point d'amure à la cible.*
        Chance de réussite 1/3
    */

    
    public function __construct(string $name) {
        parent::__construct($name);
        $this->hp = 40;
        $this->maxHp = 60;
        $this->atk = 25;
        $this->magicAtk = 2;
        $this->armor = 8;
        $this->magicArmor = 2;
    }

    public function heavyBowAttack(object $target)
    {
        if($target->isDead()) {
            echo "<p>Je vais quand même pas taper sur un macabé</p>";
        } else {
            if(rand(1,3) > 1) {
                $damage = $this->getAtk() * 2 - $target->getArmor();
                if ($damage > 0) {
                    $target->loseHp($damage);
                }
            } else {
                echo "Raté tocard ! <br>";
            }
        }
    }

    public function reduceArmor(object $target)
    {
        if(rand(1,3) == 3) {
            $targetArmor = $target->getArmor();
            $target->setArmor($targetArmor - 1);
            echo $target->getName() . " vient de perdre un point d'armure";
        }
    }
}